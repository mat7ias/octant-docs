# Terms of Use
Octant DApp
v. 1, 1 July 2023

## 1. Golem Project and GLM Governance Experiment (“Octant”)

The Golem Project (“Golem Project”) has created a global network for the exchange of computation power by connecting computers in a peer-to-peer network, enabling both application owners and individual users to rent resources from other users’ machines.

The Golem Foundation, with its registered seat in Zug, Switzerland (“**Foundation**”), has the purpose of promoting and developing new technologies, solutions, and applications within the scope of the Golem Project, including the native token of the Golem Network (“**GLM**", formerly known as GNT).

In line with its statutory purpose, the Golem Foundation launched the GLM governance experiment (named “**Octant**” or “**Octant Experiment**”, technically represented by the Octant DApp and the related smart contracts (“**DApp**”), as further set forth herein, as well as Octant’s Epoch Zero, a special pre-launch event for Octant (“**Epoch Zero**”).

These Terms of Use (“**Terms**”) govern the rights and obligations of users (“**User(s)**” when participating in the Octant Experiment and using the DApp.

## 2. Accepting the Terms of Use

These Terms, together with any documents expressly incorporated by reference, govern the access and use of the DApp and the related frontend user interface (“**Frontend**”) as well as participating in Epoch Zero. By using the Octant DApp, the Frontend and/or Octant (including Epoch Zero), the User agrees to be bound by these Terms and all the terms incorporated herein by reference.

The Foundation reserves the right to change or modify these Terms and/or the specifications of the DApp and Epoch Zero at any time in its own and sole discretion. This includes the right to terminate or substantially amend the Octant Experiment, or parts of it, at its full discretion and without notice and/or consent of the User.

All changes to the Terms and/or the Octant Experiment are effective immediately when communicated on the website or other channels chose by the Foundation.

By continuing to access or use the DApp and/or Octant (including Epoch Zero), the User confirms that he accepts the updated Terms and all of the terms incorporated therein by reference.

## 3. DApp and Access to Octant

**Accessing the DApp**: While the DApp can be openly accessed with any blockchain-explorer, the Foundation runs a Frontend available under octant.app that allows Users an easy access to Octant. Using the Frontend require User only to connect his wallet.

**Participating in the Octant Experiment**: To participate in the Octant Experiment, the User must time-lock a certain amount of GLM, as defined by the Foundation from time to time, in the DApp (“**Octant Locking**”) from a wallet controlled by the User (“**Wallet**”). Through Octant Locking, User becomes eligible to provide certain community services within the Octant Experiment. The type of services will be specified by the Foundation from time to time and may for example, consist of voting on proposals, testing technical or other features, etc.

GLM that are subject to the Octant Locking are not pooled with GLM of other Users and are held in the non-custodial smart contract. Accordingly, the Foundation has no access to and may not dispose of any GLM that are subject to Octant Locking. At the end of the time-lock, GLM used for Octant Locking may be transferred back (unlocked) to the Wallet by the User.

To participate in Epoch Zero and to vote for projects which they wish to be supported from Octant’s Epoch Zero reward pool, eligible Users must only connect its Wallet containing Epoch Zero Token (“**EZT**”) to Snapshot. The use of Snapshot is subject to Snapshot’s terms.

**Service Rewards**: For providing community services, Users may be eligible to receive a reward in ETH (“**Individual Reward**”) determined at the discretion of the Foundation. The User may claim the Individual Reward to be transferred to its Wallet or, alternatively, donate such Individual Reward (or part of it) to a public good purpose as proposed by the Foundation. In case the User decides to donate the Individual Reward (or part of it), the Foundation shall donate an additional amount of ETH as determined by the Foundation to the same public good purpose (“**Premium Reward**”).

If a User fails to claim the Individual Reward within the timeframe communicated to the User in Octant or in case an Individual Reward has been donated to a public good purpose which fails to reach the required predetermined funding threshold, the Individual Reward falls to the Foundation which may freely dispose of it.

### 3.1 Wallet

The Foundation has no control over the Private Keys of the Users. The use of the Wallet is subject to the terms and conditions of the respective third-party wallet provider. The Foundation has no custody or control of the Wallets and Users are solely responsible for the security of their Wallet.

### 3.2 Fees

**No DApp Fees**: The Use of the DApp is free of charge.

**Gas Fees**: All transactions made in relation to the Golem Network and/or the DApp, including without limitation Locking and transferring GLM, are facilitated by smart contracts on a blockchain. The blockchain requires the payment of a transaction fee (Gas Fee) for every transaction that occurs on the blockchain, and thus every such transaction requires the payments of Gas Fees as well. The value of the Gas Fee changes, often unpredictably, and is entirely outside of the control of the Foundation. User also acknowledges and agrees that the Gas Fee is non-refundable under all circumstances. It is at the sole responsibility of the Users to inform themselves about applicable fees and to pay such fees.

## 4. Intellectual Property Rights

### 4.1 Rights in the DApp, the Frontend and Octant

Every User acknowledges and agrees that the DApp, the Frontend and/or Octant, including its “look and feel” (e.g., graphics, design, text, images, logos, page headers, button icons, and scripts), proprietary content, information and other materials, and all content and other materials contained therein, including, without limitation, the Foundation logo and all designs, text, graphics, pictures, data, software, sound files, other files, and the selection and arrangement thereof are the proprietary property of the Foundation or its affiliates, licensors, or users, as applicable, and the User agrees not to take any action(s) inconsistent with such ownership interests. The Foundation and its affiliates, licensors, and users, as applicable, reserve all rights in connection with the DApp, the Frontend and/or Octant and its content, including, without limitation, the exclusive right to create derivative works. Except as expressly set forth herein, User’s use of the DApp, the Frontend and/or Octant does not grant to User ownership of any other rights with respect to any content, code, data or other materials that User may access on or through the DApp, the Frontend and/or Octant.

### 4.2 Rights to User’s Content

Every User is solely responsible for all the information User provided to the Foundation and/or published via DApp, the Frontend and/or Octant (“**User’s Content**”). By using the services of the Foundation and by making any User’s Content available on the DApp, the Frontend and/or Octant, User hereby grants to the Foundation a non-exclusive, transferable, sub-licensable, worldwide, royalty-free license to use, copy modify, create derivative works based upon, distribute, publicly display and publicly perform User’s Content in connection with operating and providing the Foundation services for any current and future business purposes of the Foundation.

Every User represents and warrants that it has and will have all rights that are necessary to grant the Foundation the license rights in User’s Content under these Terms. The User further represents and warrants that neither its User’s Content, nor its use and provision of its User’s Content to be made available through or on the DApp, the Frontend and/or Octant, nor any use of its User’s Content by the Foundation on or through the DApp, the Frontend and/or Octant will infringe, misappropriate or violate a third party’s intellectual property rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.

### 4.3 User’s License to Access and Use the DApp and/or Octant

Users are hereby granted a limited, non-exclusive, non-transferable, non-sublicensable license to access and use the services provided at the DApp, the Frontend and/or Octant, however, such license is subject to User’s compliance with these Terms.

## 5. Code of Conduct

The Foundation may issue rules for the use of the DApp, the Frontend and/or Octant and amend them at its own and sole discretion (“**Code of Conduct**”). Users are obliged to inform themselves about the current version of the Code of Conduct and assure that they comply with the Code of Conduct.

## 6. Communication

Users agree and understand that the Foundation will communicate with Users via electronic means via the Frontend. Users agree that any notices, agreements, disclosures, or other communications made in the Frontend is considered valid.

## 7. Representations and Warranties of the User

The User represents and warrants to the Foundation the following, and acknowledges that the Foundation is relying on these representations and warranties:
- User is duly organized, validly existing and in good standing under the laws of its domicile;
- User has the full right, power and authority to use the DApp and related smart contracts, the Frontend, Octant and, in Epoch Zero, Snapshot and accept this Terms;
- User owns, or has secured all rights (including intellectual property rights), consents, clearances and approvals necessary to be able to grant the rights granted hereunder;
- User is not listed, or associated with any person or entity listed, on any of the US Department of Commerce’s Denied Persons or Entity List, the US Department of Treasury’s Specially Designated Nationals or Blocked Persons Lists, the US Department of State’s Debarred Parties List, the EU Consolidated List of Persons, Groups and Entities Subject to EU Financial Sanctions, or the Swiss SECO’s Overall List of Sanctioned Individuals, Entities and Organizations, and neither the User nor any of its affiliates, officers or directors is a resident of a country or territory that has been designated as non-cooperative with international anti-money laundering principles or procedures by an intergovernmental group or organization, such as the Financial Action Task Force on money laundering; 
- User confirms not to be resident of, citizen of or located in a geographic area that is subject to UN-, US-, EU-, Swiss or any other sovereign country sanctions or embargoes;
- User has such knowledge and experience in financial and business matters that the User is capable of evaluating the merits and risks of agreeing to these Terms and using the DApp, the Frontend, Octant and/or, in Epoch Zero, Snapshot;
- User has a deep understanding of the functionality, usage, storage, transmission mechanisms and intricacies associated with cryptographic tokens, like BTC and ETH, and blockchain-based software systems;
- User has been advised that the use of the DApp and related smart contracts, the Frontend, Octant, Snapshot and the tokens to be issued and/or transferred by/to/from the User hereunder may, in certain jurisdictions, be considered securities, and that such issuance and/or transfer may be subject to securities laws. 
- User is using the DApp and/or related smart contracts, the Frontend, Octant and Snapshot in line with the Code of Conduct, if adopted and as amended from time to time, and under no circumstances for any illegal purposes;
- USER HEREBY WAIVES THE RIGHT TO PARTICIPATE IN ANY CLASS-ACTION LAWSUIT OR CLASS-WIDE ARBITRATION AGAINST ANY ENTITY OR INDIVIDUAL INVOLVED IN THE DEVELOPTMENT OR PROVISION OF THE DAPP AND/OR RELATED SMART CONTRACTS, THE FRONTEND, OCTANT AND SNAPSHOT;
- User understands and expressly accepts that there is no warranty whatsoever on the DApp and/or related smart contracts, the Frontend and Octant, expressed or implied, to the extent permitted by law, and that the use of the DApp, the Frontend, Octant and Snapshot is at the sole risk of the User on an “as is” and “under development” basis and without, to the extent permitted by law, any warranties of any kind, including, but not limited to, warranties of title or implied warranties, merchantability or fitness for a particular purpose. User is aware that, subject to Section [12], it will not receive money or any other compensation for any damages he might incur in connection with the use of the DApp, the Frontend, Octant and/or Snapshot;
- User understands and accepts that it has not relied on any representations or warranties made by the Foundation or any other person outside of those made in these Terms, including but not limited to, conversations of any kind, whether through oral or electronic communication, or any presentation, technical paper, whitepaper, social media content or website posting;
- User is of legal age in the jurisdiction applicable to him and that he has the right, authority and capacity to enter into these Terms.
- User has not granted any other party any rights that conflict with the rights granted herein.

## 8. Indemnifications

User agrees to the fullest extent permitted by applicable law, to indemnify, defend, and hold harmless the Foundation, and its respective past, present, and future employees, officers, directors, contractors, consultants, equity holders, suppliers, vendors, service providers, parent companies, subsidiaries, affiliates, agents, representatives, predecessors, successors, and assigns (individually and collectively, the “**Foundation Parties**”), from and against all actual or alleged claims, damages, awards, judgments, losses, liabilities, obligations, penalties, interests, fees, expenses (including, without limitation, attorneys’ fees and expenses), and costs (including, without limitation, court costs, costs of settlement, and costs of pursuing indemnification and insurance), of every kind and nature whatsoever, whether known or unknown, foreseen or unforeseen, matured or unmatured, or suspected or unsuspected, in law or equity, whether in tort, contract, or otherwise (collectively, “**Claims**”), including, but not limited to, damages to property or personal injury, that are caused by, arise out of or are related to (a) User’s use or misuse of the DApp, the Frontend and/or Octant, (b) any Feedback User provides, (c) User’s violation or breach of these Terms or applicable law, and (d) User’s violation of the rights of or obligations to a third party, including another User or third party, and (e) User’s negligence or wilful misconduct. User agrees to promptly notify the Foundation of any Claims and cooperate with the Foundation Parties in defending such Claims. User further agrees that the Foundation Parties shall have control of the defence or settlement of any Claims. This indemnity is in addition to, and not in lieu of, any other indemnities as set forth in a written agreement between User and the Foundation.

## 9. Disclaimers

User’s access to and use of the DApp and related smart contracts, the Frontend, Octant and/or, in Epoch Zero, Snapshot is at its own risk. User understands and agrees that the service is provided on an “as is” and “is available” basis and the Foundation expressly disclaims warranties or conditions of any kind, either express or implied. The Foundation and its officers, employees, directors, shareholders, parents, subsidiaries, affiliates, agents and licensors make no warranty or representation and disclaim all responsibility for whether the services of the Foundation: (a) will meet User’s requirements; (b) will be available on an uninterrupted, timely, secure, or error-free basis; or (c) will be accurate, reliable, complete, legal, or safe. The Foundation disclaims all other warranties or conditions, express or implied, including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, title and non-infringement. The Foundation will not be liable for any kind from any action taken or taken in reliance on material or information, contained on the DApp, the Frontend, Octant and/orSnapshot. While the Foundation attempts to make User’s access to and use of the DApp, the Frontend and/or Octant safe, the Foundation cannot and does not represent or warrant that the service is available at all times, free of viruses or other harmful components. The Foundation cannot guarantee the security of any data that User discloses online. No advice or information, whether oral or obtained from the Foundation parties or through the service, will create any warranty or representation not expressly made herein. User accepts the inherent security risks of providing information and dealing online over the internet and will not hold the Foundation responsible for any breach of security.

The liability of the Foundation for direct and indirect damages – regardless of the legal ground – is expressly excluded to the maximum extent permitted by law. Likewise, contractual liability for actions or omissions of auxiliary persons as well as non-contractual liability of the Foundation is excluded to the maximum extent permitted by law.

The Foundation will not be responsible or liable to User for any loss and take no responsibility for, and will not be liable to User for, any use of the DApp and related smart contracts, the Frontend, Octant and/or Snapshot, content, and/or content linked to or associated with any losses, damages, or claims arising from: (a) User error, incorrectly constructed transactions, or mistyped addresses; (b) server failure or data loss; (c) unauthorized access or use; (d) any unauthorized third party activities, including without limitation the use of viruses, phishing, bruteforcing or other means of attack against the service.

The Foundation is not responsible or liable for any sustained losses or injury due to vulnerability or any kind of failure, abnormal behaviour or software (e.g., Wallet, smart contract), the DApp and related smart contracts, the Frontend, Octant and/or Snapshot. The Foundation is not responsible for losses or injury due to late reports by developers or representatives (or no report at all) of any issues with the DApp and/or related smart contracts, the Frontend, Octant and/or Snapshot.

The foregoing does not affect any warranties that cannot be excluded or limited under applicable law. To the extent the Foundation may not, as a matter of applicable law, disclaim any (implied) warranty, the scope and duration of such warranty shall be the minimum permitted by applicable law.

## 10. Risks

User accepts and acknowledges the risks connected to the use of the DApp and related smart contracts, the Frontend, Octant and/or Snapshot. In particular, but not exhaustively, the User understands the inherent risks listed hereinafter. By using the DApp and/or related smart contracts, the Frontend, Octant and/or Snapshot the User acknowledges and assumes these risks:

### 10.1 Risk of Software Weaknesses

The User understands and accepts that the DApp and related smart contracts, the Frontend and/or Octant, other involved software and technology as well as technical concepts and theories are still unproven in a live (non-test-)environment, which is why there is no warranty that the process for the initiation of Experiments and issuing, receiving, use and ownership of the tokens will be uninterrupted or error-free and there is an inherent risk that the software and related technologies and theories could contain weaknesses, vulnerabilities or bugs causing, inter alia, the complete loss of the tokens.

The User particularly understands and accepts that the DApp and related smart contracts are (once deployed) fully decentralized and immutable and that, as a consequence, it may be difficult or impossible to cure software weaknesses.

The Foundation does not control the DApp and/or related smart contracts with which Users are interacting and that may be integral to Users’ ability to complete transactions on them.

User understands and accepts that in Epoch Zero, the third party governance platform Snapshot is used and that the Foundation may not be held liable for errors or defects related to the functioning of this platform.

### 10.2 Regulatory Risk

The regulatory regime governing blockchain technologies, cryptocurrencies, and tokens is uncertain, and new regulations or policies may materially adversely affect the use of the DApp and/or related smart contracts, the Frontend,Octant and/or Snapshot.

The User understands and accepts that blockchain technology allows new forms of interaction. There is a possibility that certain jurisdictions will apply existing regulations or introduce new regulations addressing blockchain technology-based applications, in a way which may be contrary to the current setup and which may, inter alia, result in substantial modifications to the DApp and/or related smart contracts, the Frontend, Octant and/or Snapshot, including the termination of the Project and the loss of the tokens or their functionality for the User.

The User understands and accepts that certain regulators may nevertheless qualify tokens as securities or other financial instruments under their applicable law. It remains in the User’s responsibility to comply with any laws and regulations applicable to the User when holding or transferring tokens.

User explicitly accepts and acknowledges that the Foundation assumes no liability for any and all risks relating to taxation or regulatory issues in connection with the use of the DApp and/or related smart contracts, the Frontend, Octant and/or Snapshot. It remains in the sole responsibility of the User to seek local legal, tax and regulatory advice.

### 10.3 Risk of Abandonment / Lack of Success

A lack of use or public interest in the creation and development of distributed ecosystems could negatively impact the development of those ecosystems and related applications and could therefore also negatively impact the potential utility of the DApp and/or related smart contracts, the Frontend and/or Octant.

### 10.4 Risk of third party provider

The services of the Foundation may rely on third party software. Furthermore, in Epoch Zero, Snapshot and thus a third party governance platform is used. If the Foundation is unable to maintain a good relationship with such third parties; if the terms and conditions or pricing of such third parties change; if the Foundation violates or cannot comply with the terms and conditions of such third parties; or if any of such third parties loses market share or falls out of favour or is unavailable for a prolonged period of time, access to and use of the DApp and/or related smart contracts, the Frontend, Octant and/or Snapshot may suffer.

### 10.5 Risk of Private Key Loss

Tokens allocated to the User’s address can only be accessed with the User’s access data and/or private key. The User understands and accepts that if its private key file or Wallet password were lost or stolen, the access to the User’s tokens allocated to the User’s address would be unrecoverable and would be permanently lost. The Foundation has no control over the User’s tokens; therefore, the User shall have no recourse to seek any refunds, recovery or replacements from the Foundation in the event that he cannot access the User’s Wallet anymore and/or any tokens are lost or stolen.

### 10.6 Risk of Theft

The User understands and accepts that, while best efforts are made to reduce potential software attacks on the DApp and/or related smart contracts, the Frontend and/or Octant, other involved software, and/or other technology components may be exposed to attacks by hackers or other individuals that could result in theft or loss of the tokens.

### 10.7 Risk of Protocol Attacks and Forks

The User understands and accepts that, as with other blockchains, the blockchain used for the Golem Network and/or Octant could be susceptible to consensus-related attacks, including but not limited to double-spend attacks, majority validation power attacks, censorship attacks, and byzantine behaviour in the consensus algorithm or be subject to forks. Any successful attack or fork presents a risk to the Golem Network, the DApp and/or related smart contracts, the expected proper execution and sequencing of token-transactions and the expected proper execution and sequencing of contract computations as well as the token balances in the wallet of the User.

There are risks associated with using Internet and blockchain based products, including but not limited to, the risk associated with hardware, software and internet connections, the risk of malicious software introduction, and the risk that third parties may obtain unauthorized access to User’s information. User accepts and acknowledges that the Foundation will not be responsible for any communication failures, disruptions, errors, distortions or delays User may experience when using the DApp and/or related smart contracts, the Frontend, Octant and/or Snapshot for transactions, however caused.

## 11. Data Privacy Policy

The personal data provided by the User in connection with these Terms (“**User Information**”) shall not be transferred by the Foundation or any other person or entity engaged or controlled by the Golem Foundation that may have access to such User Information unless such transfer is required to be made to i) legal and tax advisers of the Golem Foundation or ii) governmental entities or Service Providers (e.g. banks or KYC providers) that are subject to respective secrecy provisions regarding the User Information received. 

By voluntarily providing personal data to the Foundation or any other person or entity assigned by the Foundation to collect such data, the User is consenting to the use of it in accordance with this Section 12 and the applicable data protection laws. The User in providing personal data to the Foundation or any other person or entity assigned by the Foundation to collect such data, acknowledges and agrees that such personal data may be transferred from their current location to the offices and servers of the Foundation and the authorized third parties, some of whom may be located outside of the User’s country. As far as necessary for the fulfilment of regulatory and compliance obligations, the User’s personal data may be transmitted to third parties, e.g., to banks etc. The Foundation will only transfer personal data to countries for which the EU Commission or the Swiss Federal Data Protection and Information Commissioner (FDPIC) has decided that they have an appropriate level of data protection, or the Foundation will implement measures to ensure that all recipients comply with an appropriate level of data protection.

The Foundation or any other person or entity assigned by the Foundation to collect such data uses reasonable physical, electronic, organizational and procedural safeguards to protect the personal information obtained from the User from loss, misuse, and unauthorized access, disclosure, alteration, and destruction. Please note that the Foundation or any other person or entity assigned by the Foundation to collect such data is not responsible for the security of any data transmitted over the Internet, or any data stored, posted, or provided directly to a third party’s website, which is governed by that party’s policies. Please note that no method of transmission over the Internet or method of electronic storage is 100% secure.

The time periods for which the Foundation retains personal data depends on the purposes for which it is used. The Foundation or any other person or entity assigned by the Foundation to collect such data may retain information about the User in their databases for as long as needed to provide the described services and in accordance with applicable laws. The retention and use of personal information by the Foundation or any other person or entity assigned by the Foundation to collect such data will be required to comply with legal obligations, resolve disputes, and enforce agreements. The retention period may extend beyond the end of the relationship between the parties, but it will be only as long as it is necessary for the Foundation to have sufficient information to respond to any issues that may arise later. For example, the Foundation or any other person or entity assigned by the Foundation to collect such data may need or be required to retain certain information to prevent fraudulent activity, protection against liability, permit itself to pursue available remedies or limit any damages that the Golem Foundation or any other person or entity assigned by the Foundation to collect such data may sustain, or if a law, regulation, rule or guideline requires it.

The Foundation will respond to a request for access to information collected about the User within the time frame required by applicable law. Any such requests shall be made exclusively to: contact@golem.foundation.

## 12. Modifications to the DApp

The Foundation reserves the right in its sole discretion to modify, suspend, or discontinue, temporarily or permanently, the provision of the DApp (or any features or parts thereof) at any time and without liability as a result.

## 13. Tax Considerations

Users are solely responsible for determining what, if any, taxes apply to their use of the DApp and/or related smart contracts, the Frontend and/or Octant. The Foundation is not responsible for determining or paying the taxes that apply to such use.

## 14. Miscellaneous

### 14.1 Relationship of the Parties

The Foundation and the User are independent contractors. These Terms do not create nor is it intended to create a partnership, franchise, joint venture, agency, fiduciary or employment relationship between the Parties.

### 14.2 Severability

If any provision of this Terms is invalid, illegal or unenforceable in any jurisdiction, such invalidity, illegality or unenforceability shall not affect any other provision of the Terms or invalidate or render unenforceable such provision in any other jurisdiction. Upon such determination that any provision is invalid, illegal or unenforceable, the Terms shall be modified to effectuate the original intent of the original provision as closely as possible.

### 14.3 Assignment

User may not assign or transfer any rights and licenses granted under these Terms, without prior written (text form sufficient) consent of the Foundation. The Foundation may freely assign or transfer any rights and licenses granted under this Terms without restriction.

### 14.4 Applicable Law and Jurisdiction 

These Terms as well as the use of the DApp and/or related smart contracts, the Frontend and/or Octant shall be governed by and construed in accordance with the laws of Switzerland without giving effect to any choice or conflict of law provision or rule (whether of Switzerland or any other jurisdiction). The application of the United Nations Convention on Contracts for the International Sale of Goods shall be excluded.

Any dispute arising out of or in conjunction with these Terms shall be submitted to the exclusive jurisdiction of the courts of the canton of Zug, Switzerland, although the Foundation retain the right to bring any suit, action, or proceeding against User for breach of these Terms in its country of residence or any other relevant country. Users waive any and all objections to the exercise of jurisdiction by such courts and to the venue in such courts.
