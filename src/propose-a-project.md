# Propose a project

Octant is a community-driven platform for experiments in decentralized governance. During its MVP stage we will be using Octant to test various hypotheses related to sustainable financing of public goods and infrastructure. For every 90-day epoch, a range of public good projects will be chosen as potential donation recipients. 

[Golem Foundation][foundation] will be maintaining and curating the list of projects eligible to receive funds from the Octant reward pool, based on inputs from the Octant community.

To be considered for inclusion, potential donation recipients should meet the following set of criteria:
- commitment to open-source technology and sharing results publicly;
- transparency about how exactly funding will be used;
- advancing values of freedom and privacy (no surveillance and handling of personal data);
- supporting decentralization in various fields (for example, building [Web3 projects][web3]);
- having social proof, being recognizable in the area the project is being developed in.

Special consideration will be made for [GLM][glm], and [Golem Network][golem-network]-compatible projects meeting the above listed criteria.

Exclusion criteria:
- being a for profit project;
- being a financial (lending, investing, trading) product.

Every Octant user can submit proposals for new additions to the list of eligible projects, a change in the eligibility criteria, or a theme for an epoch. 

Once a cause or an organization is listed on the eligibility index, it can be selected as one of the potential beneficiaries in a given epoch (depending on its theme). There are no limits to the number of times a project can be a potential beneficiary.

The voting for donation recipients to be included in an epoch will happen on Octant's [Snapshot][snapshot]. Only GLM holders who donated to projects shortlisted in the previous funding round are eligible to vote.

A detailed description of the submission process is available [here](./submission-process.md).

[foundation]: https://golem.foundation
[web3]: https://ethereum.org/en/web3/
[glm]: https://www.golem.network/glm 
[golem-network]: https://golem.network
[snapshot]: https://snapshot.org/#/octantapp.eth
