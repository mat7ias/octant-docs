const title = "Octant documentation";
const description = "Octant is a community-driven platform for experiments in decentralized governance.";
const author = "Golem Foundation";
const ogprefix = "og: http://ogp.me/ns#";

module.exports = {
  base: "",	
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title,
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ["meta", { prefix: ogprefix, property: "og:title", content: title }],
    ["meta", { prefix: ogprefix, property: "twitter:title", content: title }],
    ["meta", { prefix: ogprefix, property: "og:type", content: "article" }],
    ["meta", { prefix: ogprefix, property: "og:image", content: "https://docs.octant.app/assets/img/Allocation-summary.8cb491df.png" }],
    ["meta", { prefix: ogprefix, property: "twitter:card", content: "summary_large_image" }],
    [
      "meta",
      {
        prefix: ogprefix,
        property: "og:url",
        content: "https://docs.octant.app",
      },
    ],
    [
      "meta",
      { prefix: ogprefix, property: "og:description", content: description },
    ],
    [
      "meta",
      { prefix: ogprefix, property: "og:article:author", content: author },
    ],
    ["meta", { name: "theme-color", content: "#3eaf7c" }],
    ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
    [
      "meta",
      { name: "apple-mobile-web-app-status-bar-style", content: "black" },
    ],
    [
      "link",
      { rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon.png" },
    ],
    ["apple-touch-icon", { sizes: "512x512", href: "/favicon-512.png" }],
    ["link", { rel: "preconnect", href: "https://fonts.gstatic.com" }],
    [
      "link",
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap",
      },
    ],
    [
      "script",
      {},
      `document.body.addEventListener("click", (e) => {
        if (e.target.classList.contains("logo")) {
          e.preventDefault();
          window.location = "http://octant.build"
        }
      })`,
    ],
    [
      "script",
      {
      src: "https://analytics.golem.foundation/gftracker.js",
      "data-ackee-server": "https://analytics.golem.foundation",
      "data-ackee-domain-id": "8fa710b3-b341-4bc6-b334-8f944c1e2afa",
      "async": "async"
      }
    ],
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: "",
    editLinks: false,
    docsDir: "",
    editLinkText: "",
    lastUpdated: false,
    logo: "/logo.png",
    nav: [
      {
        text: "Launch Octant",
        link: "https://octant.app",
      },
    ],
    sidebar: [
      {
        title: "Introduction",
        path: "/",
      },
      {
        title: "How Octant works",
        path: "/how-it-works",
      },
      {
        title: "Using the app",
        path: "/using-the-app",
        sidebarDepth: 1,
      },
      {
        title: "GLM-locking mechanism",
        path: "/glm-locking",
      },
      {
        title: "Propose a project",
        path: "/propose-a-project",
      },
      {
        title: "Submission process",
        path: "/submission-process",
      },
      {
        title: "Tips for projects",
        path: "/tips-for-beneficiaries",
      },
      {
        title: "Technical outline",
        path: "/technical-outline",
      },
      {
        title: "Beyond the MVP",
        path: "/beyond-the-mvp",
      },
      {
        title: "FAQ",
        path: "/faq",
        sidebarDepth: 1,
      },
      {
        title: "Octant Paper ",
        path: "https://octantapp.notion.site/Octant-a-GLM-Governance-Experiment-e098d7ff9d55468db28b8b3584b5959c",
      },
      {
        title: "Source Code ",
        path: "https://gitlab.com/golemfoundation/governance/octant/",
      },
      {
        title: "Security Audit Report ",
        path: "https://leastauthority.com/blog/audits/audit-of-golem-foundations-octant-smart-contract/",
      },
      {
        title: "Discord ",
        path: "https://discord.gg/octant",
      },
      {
        title: "Snapshot ",
        path: "https://snapshot.org/#/octantapp.eth",
      },
      {
        title: "Terms of Use ",
        path: "/terms-of-use",
      },
      {
        title: "© 2023 Golem Foundation Zug, Switzerland",
      },
    ],
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    "@vuepress/plugin-back-to-top",
    "@vuepress/plugin-medium-zoom",
    [
      "vuepress-plugin-mathjax",
      {
        target: "svg",
        macros: {
          "*": "\\times",
        },
      },
    ],
  ],

  markdown: {
    extendMarkdown: (md) => {
      md.use(require("markdown-it-footnote"));
    },
  },
};
