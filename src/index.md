# Introduction

Octant is a novel platform for experiments in participatory public goods funding, centered on Golem's native ERC-20 token, [GLM][glm].

Developed by the [Golem Foundation][golem-foundation] to explore motivations and behaviors that drive engagement in funding public goods, Octant uses recurring funding rounds and rewards active user participation with ETH.

## Key points:

- Golem Foundation stakes 100,000 ETH to help the Ethereum network reach consensus. Every 90 days, a fraction of the staking rewards is used to fund the Octant reward pool.
- The Octant reward pool is split into two streams: User Rewards and Matched Rewards.
- To participate in the distribution of Octant funds, users must lock a minimum of 100 [GLM][glm] into the Octant contract.
- Users with an [effective locked balance][effective] of at least 100 GLM become eligible for User Rewards.
- Rewards for users are calculated using a time-weighted average. The greater the volume of GLM locked and the longer its duration, the higher the reward. For optimal rewards, users should lock as much GLM as they are comfortable with and maintain that lock in Octant throughout the entire 90-day epoch.
- The [GLM time-locking mechanism][glm-locking] is non-custodial. Octant users retain full control of their tokens and can withdraw their GLM at any time. Lowering the lock-in amount during an epoch will recalibrate the user's time-weighted average to reflect the smallest locked amount. If the effective locked balance falls below 100 GLM, no rewards will be calculated.
- At the beginning of a new epoch, users with valid lock-ins will have a two-week-long decision window during which they can claim their User Rewards for themselves or donate their share of Octant rewards, in part or in full, to one or more eligible public goods projects. The impact of each individual donation will be magnified through Matched Rewards.
- The list of projects eligible for Octant funding is curated by Golem Foundation, based on inputs from the Octant community. [Every community member can suggest new additions to the list, a change in eligibility criteria, or a theme for an epoch][propose-a-project].

## The flow of funds in Octant

![](./images/hex2.gif)

## Current status

Octant officially launched on the Ethereum mainnet on [August 8th, 2023][launch]. 

Our first Allocation Window was active from October 19 to November 2, 2023. During this period, the Octant community [successfully allocated a total of 330.25 ETH in rewards, directing 250 ETH to support 19 high-impact public goods projects][firstAW]. 

The Epoch Two Allocation Window was open from January 17th to January 31, 2024. [Out of the 365 ETH Total Reward budget, 134 ETH went to users and 231 ETH to 19 high-impact public goods projects][twitter]. An in-depth review of the allocation window will be published soon on the [Octant blog][blog].  

We are now in Epoch Three, with the upcoming Allocation Window scheduled to begin on April 16, 2024. **New users are welcome to join at any time.**

In Epoch Three, Octant is getting a major algorithm makeover. Check out all the details of this overhaul [here][overhaul].

If you are already familiar with Octant and want to jump right into the DApp, see the [Using the App guide][using-the-app].

If you want to know more about the Octant rationale and the overall design, read the [How Octant works][how-it-works] section of the docs.

To know more about how Octant total reward pool, users’ rewards, and matched rewards are calculated, check the Octant [Technical Outline][technical-outline] or read [Octant paper][octant-paper].

[golem-foundation]: https://golem.foundation
[glm]: https://www.coingecko.com/en/coins/golem
[effective]: ./faq.md#what-does-effective-locked-balance-mean
[glm-locking]: ./glm-locking.md
[propose-a-project]: ./propose-a-project.md
[launch]: https://golem.foundation/2023/08/08/announcing-octant.html
[firstAW]: https://golem.foundation/2023/11/10/first-allocation-window-results.html
[twitter]: https://twitter.com/OctantApp/status/1752744706671575343
[blog]: https://blog.octant.build
[overhaul]: https://blog.octant.build/octants-algorithm-overhaul/
[using-the-app]: ./using-the-app.md
[how-it-works]: ./how-it-works.md 
[technical-outline]: ./technical-outline.md
[octant-paper]: https://octantapp.notion.site/Octant-a-GLM-Governance-Experiment-e098d7ff9d55468db28b8b3584b5959c
