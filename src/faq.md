# Octant FAQ

## Is Octant related to the Golem compute project?

The organization leading Octant is [Golem Foundation](https://golem.foundation), which was created from the [Golem compute project](https://golem.network). You can read about this transition in this [post](https://golem.foundation/2019/06/28/introducing-golem-foundation.html).

## Why do you use the GLM token?

The [GLM token](https://www.golem.network/glm) is the native token to the Golem ecosystem. The [community crowdfunded the project in 2016](https://blog.golemproject.net/the-golem-crowdfunding-summary/) with an ETH-GLM swap.

## Can we use another token?

At this time GLM is the only token you can use to engage with Octant.

## Where do I get GLM tokens?

That might depend on your jurisdiction, but GLM is widely available on most exchanges, decentralised and otherwise. You can view all of the markets where you can acquire GLM [here](https://coinmarketcap.com/currencies/golem-network-tokens/markets/).

## How is Octant funded?

Golem Foundation contributes by staking 100,000 ETH from its treasury. Every 90 days, a portion of these staking rewards is allocated to the Octant reward pool.

## How is the size of the Octant reward pool determined?

Initially, the transfer of staking rewards from the Golem Foundation to Octant depended on [the percentage of the total GLM supply locked by Octant users](https://octantapp.notion.site/Octant-a-GLM-Governance-Experiment-e098d7ff9d55468db28b8b3584b5959c) — the more GLM locked in the Octant contract, the greater the portion of the Foundation’s staking rewards directed to the Octant community. However, [in Epoch Three, we shifted to a predetermined split](https://blog.octant.build/octants-algorithm-overhaul/):
- 70% of the staking yield contributes to Octant's Total Rewards budget, split evenly between Individual Rewards and Matching Rewards.
- 25% of the staking yield goes to the Golem Foundation, helping cover expenses like validator management, Octant administration, development, marketing, and other initiatives.
- 5% of the staking yield bolsters the Octant Community Fund.

## How are User Rewards calculated?

Your rewards are based on two things: the amount of GLM you lock and how long you keep it locked. Simply put, if you lock 1% of the total GLM supply, you'll earn 1% of the staking reward. But timing matters due to Octant's time-weighted average system. As an example, if you begin with 100 GLM and add another 1000 GLM halfway through the epoch, the rewards will reflect an average of these amounts. 

For optimal rewards, lock as much GLM as you're comfortable with and maintain that lock in Octant throughout the entire 90-day epoch.

To get a clearer picture of potential rewards, check out Octant’s [in-app Reward calculator](./using-the-app.html#getting-started). It'll guide you in making smart locking decisions.

## Are there any fees associated with locking, unlocking, allocation decisions, and withdrawals?

Locking, unlocking, and rewards withdrawals happen on-chain; therefore, there are gas fees involved in executing them. Allocation decisions are conducted off-chain and are free. You can change your allocation decision as many times as you wish while the allocation window is open, without incurring any financial costs. 

To ensure tamper-proofing and transparency of the allocation decisions, [we provide access to our server code in open-source form](https://gitlab.com/wildland/governance/octant) and will share the voting results after closing the allocation window. This will allow the results of the allocation process to be reproduced locally and compared with those received from us.

## Can I unlock my GLM tokens at any time?

Yes, [Octant’s GLM time-locking mechanism is non-custodial](./glm-locking.md). You retain full control over your funds and can unlock your GLM at any time. However, please be aware that if your effective locked balance drops below 100 GLM, no rewards will be calculated.

## What happens if I lower my lock-in before the end of an epoch?

If you reduce your lock before the current epoch ends, your time-weighted average will adjust to the smallest locked amount. Additionally, please note that you need an effective locked balance of at least 100 GLM to earn Octant rewards.

## What does "effective locked balance" mean?

It's the amount of your locked GLM that qualifies for user rewards. Right now, that's set at 100 GLM. You can check this balance in the Earn tab of the Octant app.

## Why is there a discrepancy between my "Current locked balance" and the "Effective locked balance"?

Octant uses a time-weighted average system. The earlier you lock in your GLM during the epoch, the more aligned these two values will be. For users who lock in their GLM at the epoch's commencement and maintain that amount consistently, the current and effective locked balances will match.

## I locked GLM in Octant but saw no rewards. Why?

If you didn't receive rewards after locking GLM in Octant, it's probably because your effective locked balance was under the 100 GLM threshold. To boost your chances of getting rewards next time, consider locking a larger amount of GLM for the upcoming epoch or retaining them in Octant for a longer duration.

## Can I propose a potential Octant beneficiary project?

Yes, absolutely. The process for this is outlined [here](./propose-a-project.md). 

Please note that being shortlisted as a potential beneficiary is just the first step to receiving funding. To attract donations, public goods projects included in the funding round will need to [engage with the Octant community, promote their projects and missions, and actively contribute to vibrant discussions](./tips-for-beneficiaries.md).

## Are there any limits to the number of eligible projects to which one can allocate rewards?

You can distribute your user rewards between as many eligible projects as you like.

## What happens to the funds donated to projects that don’t pass the funding threshold?

ETH rewards allocated to projects that didn’t pass the funding threshold will go back to the Golem Foundation. You can check the current status of your shortlisted projects in the Allocate view of the Octant app. You are free to change your allocations as many times as you like before the decision window closes.

## What happens if I miss an allocation window?

If you neither donate nor claim your user rewards before the allocation window ends, the funds will be transferred to the Golem Foundation. Please consider joining our [Discord](https://discord.gg/octant), and following us on [Twitter](https://twitter.com/octantapp) to make sure you never miss an allocation window. We’ll be using these channels to inform our community about all upcoming dates of interest. Octant’s allocation windows occur every 90 days and last two weeks. If you follow our reminders, you should have plenty of time to make your allocation decisions.

## Is there a test version of the Octant app I can use before committing my funds?

Certainly! You can access a test version of Octant on the [Sepolia Testnet](https://testnet.octant.app). To give it a spin, add Sepolia to your [Metamask wallet](https://metamask.io/) using the instructions from this [guide](https://www.coincarp.com/chainlist/sepolia/).
 
To proceed, you'll need TestGLM. Acquire them by visiting our [Etherscan Faucet](https://sepolia.etherscan.io/address/0xD380d54df4993FC2Cae84F3ADB77fB97694933A8), selecting the `Contract` tab, then `Write Contract` followed by `Connect to Web3`. You'll be prompted to connect your Metamask wallet. Note: To get TestGLM, your wallet should contain some Sepolia ETH, available for free from the [Sepolia Faucet](https://sepoliafaucet.com/).

After connecting your wallet, select the`sendMeGLM` function and choose `Write`. Metamask will ask for transaction approval. Upon confirming, you'll shortly receive your TestGLM. To display them in your wallet, manually add TestGLM to your wallet by selecting `Import tokens` and entering the token contract address `0x71432DD1ae7DB41706ee6a22148446087BdD0906` and token decimal: `18`. Now, you can [launch Octant](https://testnet.octant.app), and start testing.

## Octant is using its own staking solution, where can I read more about the technical aspects of this?

We are [solo staking](https://ethereum.org/en/staking/solo/). Our current setup utilizes a *geth* and *nimbus* client pair, and our system runs on a number of hardened [Qubes OS](https://www.qubes-os.org/) boxes which enhance security through isolating the network stack from clients, among other techniques. We leverage widely-used monitoring tools, including *prometheus*, *grafana*, and *beaconchain*, to keep an eye on system performance and maintain optimal operation.

## How can I check the status of Golem Foundation's validators?

At the moment, the easiest way to check the status of our validators is by using the [*beaconchain* API](https://beaconcha.in/api/v1/docs/index.html#/Validator/get_api_v1_validator_eth1__eth1address).

Chose the `/api/v1/validator/eth1/{eth1address}` option, click on the "Try it out" button, enter the `0x4f80Ce44aFAb1e5E940574F135802E12ad2A5eF0` address and click "Execute" to see the list of our validators. 

We are working on our own validator endpoint that will make this information much more easily available to the Octant community. 

## Has Octant undergone a security audit?

Yes, our smart contracts were audited by the [Least Authority](https://leastauthority.com/). The auditors did not identify any issues in the current design and implementation of Octant's smart contracts. You can read their full report [at the Least Authority website](https://leastauthority.com/blog/audits/audit-of-golem-foundations-octant-smart-contract/).

## What’s the role of the community in Octant?

We see the development of Octant as an interactive, participatory process. We will continuously seek ways to improve the platform's design, consulting our choices with the community. In the first stage of the project, Octant users can engage in the following activities:
- funding public goods projects through individual allocations,
- proposing eligibility criteria for potential donation recipients,
- submitting potential donation recipients,
- proposing experiment designs and governance mechanisms.

## How can I get involved in the Octant community?

The quickest way to become involved in the Octant community is by joining our [Discord](https://discord.gg/octant). You can engage with other community members there, ask questions, submit proposals, and discus governance ideas with like-minded people.

## What if my question isn’t answered in the FAQ?

If you still have lingering questions or uncertainties about Octant, please refer to our [Documentation](https://docs.octant.app) for a thorough overview of the project. Feel free to engage with us on our [Discord](https://discord.gg/octant) as well. 

To contact Golem Foundation use the email address listed at this [site](https://golem.foundation/).
