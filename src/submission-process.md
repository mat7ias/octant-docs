# Project Submission Process

:memo: **Note:**
The rules and steps outlined in this document apply exclusively to new projects that have not participated in previous Octant epochs. **The rules for returning projects are under revision and will be published here prior to the opening of Epoch 3 submissions.**
:memo: 

If you're looking to secure funding in the upcoming Octant funding Epoch, known as the "Allocation Window," follow the steps outlined in this document. **First, ensure your project meets [Octant's funding eligibility criteria](./propose-a-project.md).**

### Step 1: Join the Octant Discord Server

Engage with our community by [joining our Discord server](https://discord.gg/octant). 

Start by completing our captcha verification in the [_captcha_verification!](https://discord.com/channels/1066118789479010354/1171384876558200852) channel to gain full access to community channels.

Our Discord is a dynamic space for exchanging ideas, resolving queries, and starting discussions about projects, promoting collaboration. Introduce yourself in the [Introductions](https://discord.com/channels/1066118789479010354/1066129965566926898) channel.

Being actively engaged with the Octant community on Discord is key to your success in securing potential funding through Octant.

### Step 2: Complete Submission Form and Temp-Check

Submit your project details through the [Octant Applications form](https://octant.fillout.com/t/u4uT8WFzDvus). Once submitted, directly message the [Octant_Submissions](https://discordapp.com/users/1148989551453413418) Discord account. This initiates a review of how well your proposal aligns with our mission and starts a guidance conversation. Feel free to include any questions you have.

Filling out the [Octant Applications form](https://octant.fillout.com/t/u4uT8WFzDvus).is mandatory. Ensure you provide all necessary project details as this form will form the foundation of your submission.

**Failure to complete this step before [temp-checks deadline](./submission-process.md#key-dates-and-deadlines) means you cannot move forward with securing funding in the current epoch.**

### Step 3: Community Discussion and Formalized Submission

After a productive conversation with our team through Octant_Submissions, proceed to formalize your proposal on the [Octant Discuss](https://discuss.octant.app/) forum. Your Discuss post should polish your initial proposal, incorporating feedback received from the Octant team based on your application form. For format guidance, look at past submissions or feel free to ask us; we're here to assist and want to see your public goods project succeed!

Next, create a post in the Discord [project-submissions forum](https://discord.com/channels/1066118789479010354/1151450371110219798) featuring your project's title, a tag, a TL;DR, and a link to your Discuss post.

The aim is to engage the community to boost your project's likelihood of funding. Hosting events for deeper engagement is recommended, and Octant is here to support your efforts.

**Ensure you complete this step and the [Project Information Submission](./submission-process.md#project-information-submission) by the [Project Information Submission deadline](./submission-process.md#key-dates-and-deadlines).**

### Step 4: Community Voting

The community will engage in a Snapshot vote to determine the shortlist for the upcoming Allocation Window. This stage offers you an opportunity to showcase your project's strengths and rally community support.

## Project Information Submission

All new projects that have filled out the Submission Form and finalized their proposals are required to submit their project details for potential inclusion in the [Octant App](https://octant.app).

This information must be sent to [Octant_Submissions](https://discordapp.com/users/1148989551453413418) by the [date indicated below](./submission-process.md#key-dates-and-deadlines). Most of this information should be readily available from your existing submission.

1. A text description of the project, about 3-4 paragraphs long,
2. A hero message - a concise one-liner summarizing your project,
3. A project mark / logo in **SVG format**,
4. At least one URL (to your project's website or a social media page),
5. A designated admin wallet address and a message signature from that address including the word "Octant". You can use any message signing option that you prefer. Options include:
    - [Signer.is](https://signer.is/sign)
    - [MyCrypto](https://app.mycrypto.com/sign-message)
    - [MyEtherWallet](https://www.myetherwallet.com/wallet/sign)

For a glimpse of how your project might be presented, visit the [Octant App projects section](https://octant.app/#/projects). The ETH address will be displayed in the project's URL.

Projects from previous Epochs are also welcome to update their information in the Octant App by the same Project Information Submission Deadline.

## Key-Dates and Deadlines

- **Temp-Checks:** February 12th, 2024 - March 14th, 2024
- **Project Information Submission Deadline:** March 17th, 2024
- **Snapshot Vote:** March 18th, 2024 - March 22nd, 2024
- **Allocation Window:** April 14th - April 30th, 2024
