# Using the app

:memo: **Note:**
This guide provides a comprehensive walkthrough of the user-flow. Note that some features mentioned here are only available during an allocation window.

Outside of these windows, the Octant app provides functionalities such as:

- Locking and unlocking GLM

- Scrolling through the list of potential beneficiaries

- Reviewing outcomes from past epochs

- Modifying various app settings (e.g., opting for ETH or a selected fiat currency as the primary display)

- Monitoring real-time metrics related to the Golem Foundation's staked ETH and the GLM amounts locked by Octant users.

Should you encounter any issues while using the Octant app, we encourage you to seek assistance on our dedicated [Discord channel][discord-help].

Additionally, we've established a separate [Discord channel specifically for UI and UX feedback][discord-feedback]. 
:memo: 

## Getting started

The first thing you need to do after [launching the app][launching-octant] is to connect your [WalletConnect-compatible wallet][walletconnect] to it.

To connect your wallet click on the Connect wallet button on the top of the screen and follow the instructions in your wallet.

In the onboarding process, you'll be prompted to review the [Terms of Use][terms-of-use] and indicate your agreement by clicking the checkbox. Additionally, you'll be asked to sign a message in your wallet to confirm your acceptance of these terms. Rest assured, this action won't initiate any transactions.

Once you have connected your wallet, you will have to [lock some GLM][glm-locking] in the Earn tab. [GLM][glm] is an ERC-20 Ethereum-based token native to the [Golem Network][golem-network]. If you do not have any GLM you can acquire them at various [exchanges][exchanges] and on [Uniswap][uniswap]. 

While you can lock as little as 1 GLM, an [effective locked balance][effective] of at least 100 GLM is necessary to qualify for user rewards. Your rewards will be influenced by a time-weighted average, meaning the more GLM you lock and the longer they remain locked, the more significant your reward.

<video src="./videos/lockGLM.mp4" playsInline controls></video>

If you're interested in estimating the rewards you can earn by locking a specific amount into Octant, the in-app Reward calculator is at your disposal. This tool provides an estimate of potential rewards based on your locked-in GLM amount and duration. To use it, click on the calculator icon atop the Locked Balance tile and input your GLM quantity and desired locking period.

![](./images/Calc.gif)

For a deeper dive into how rewards and matched funds are callculated, refer to Octant’s [Technical Outline][technical-outline]

Be aware that both locking and unlocking GLM occur on-chain, and executing these actions will incur gas fees.

Octant's GLM time-locking mechanism is non-custodial. You are in full control of your funds. You can unlock your GLM at any time. Please note, however, that if you reduce your lock before the current epoch ends, your time-weighted average will adjust to the smallest locked amount.

You can access your Octant-connected wallet at any time by clicking on the tile showing your Ethereum address at the top of the screen.

The Wallet view allows you to check your connected wallet balances in ETH, GLM, and their fiat equivalents in the currency of your choice (currency data is fetched using [CoinGecko][coingecko] API). The view also displays your rewards budget, as well as pending allocations and withdrawals.

The Wallet view also allows you to disconnect your wallet from Octant at any time.

## The main app views

<video src="./videos/overview.mp4" playsInline controls></video>

The app has 5 main views, which you can access by clicking at the appropriate tab at the bottom of the screen:
- Projects - where you can scroll through the list of projects to which you can donate in the current epoch;
- Allocate - which allows you to allocate your user rewards, in part or full, to projects you shortlisted;
- Metrics - which shows you a range of stats about the current and previous epochs to help you make allocation decisions;
- Earn - where you can edit your GLM lock-in, withdraw your user rewards in ETH, and check current and estimated reward stats;
- Settings - which allows you to access various app settings. The main toggle lets you to choose ETH or a selected fiat as a primary display currency.

## Allocate your Octant funds

At the end of an epoch, if you have earned rewards, you will see a slider in the Allocate view. This slider allows you to determine how much of the funds you'd like to retain for yourself and how much you'd like to donate. Adjust the slider to choose the option that suits you best. Don't worry if you change your mind later; you can modify your choice throughout the duration of the allocation window.

If you want to allocate all your User Rewards to yourself, set the slider to 100%. If you wish to donate part of your rewards to shortlisted projects, adjust the slider accordingly.

After deciding how much you want to donate, proceed to the Projects view and browse the list of public goods causes, shortlisted with the assistance of the Octant community for funding in the current allocation window (information on the eligibility criteria and on how users can submit a project can be found [here][propose-a-project]).

If you come across a project you’d like to support with your User Rewards, click on the Heart button next to it. All projects you consider for allocation will then appear in the Allocate tab.

![](./images/Add-to-allocate.gif)

Should you decide to remove a project from your shortlist in the Allocate view, you can slide or click and drag the project tile to reveal the remove button. (Please note that in the app's current version, you can't remove projects to which you've already allocated in this manner. To do so, you'll need to allocate zero to the project and confirm it in your wallet. We plan to enhance this functionality in a future update).

![](./images/Remove-projects.png)

In the Allocate view, you'll find the projects you've personally shortlisted, complemented by relevant metrics to guide your allocation choices. 

Below each project's name, you'll see two figures: the total funds required for the project to be funded and the amount already donated by Octant users. If the community's donated amount is in red, it indicates the project hasn't yet met its funding threshold. Keep in mind that [ETH rewards allocated to projects that didn’t pass the funding threshold will go back to Golem Foundation][how-it-works].

There are two ways to allocate funds to projects:

- Use the in-app slider for auto-allocation among all your chosen projects (including yourself, if you opt to retain part of your User Reward).
- Click the donation amount field on a project tile to edit it directly, switching to manual edit mode, as indicated by the 'manual' badge on the slider tile.

![](./images/Allocate-view.png)

In manual mode, you can allocate any amount within your budget to any project. If you attempt to allocate more than available, the input fields will shake and reset.

Regardless of the allocation mode, the app provides a live preview of your donation's impact. Project tiles update with each change, giving you an immediate view of the potential impact.

After adjusting your donation, the app displays the combined impact of your donation and the current matching funding. If the project is below the threshold, the line graph will update to reflect how your donation contributes.

Once you finalize your allocation, a summary appears in the Allocate view. To modify it, simply click or tap 'Edit,' adjust as needed, and then re-confirm in your wallet.

![](./images/Allocation-summary.png)

## Patron mode

If you're unsure about which projects to back but still want to help, we have a simple solution. By using the Patron mode, you can send all your rewards to the Octant Matching Fund. This helps boost community donations by increasing the donation amount.

It's vital to be aware of certain nuances associated with this mode:
- Once activated, users cannot personally claim or distribute rewards.
- Allocation choices made prior to switching on the mode become void.
- The Patron mode persists across future allocation windows unless deactivated.

To toggle Patron mode on or off, simply navigate to the Settings view within the Octant app and use the provided toggle.

## Locking and withdrawing funds

The Earn tab allows you to edit your GLM lock-in, withdraw your user rewards in ETH, and check your transaction history.

Your Current and [Effective][effective] epoch lock will be displayed there. By clicking on the Edit GLM Lock button you can change the amount of tokens you would like to lock in. Choose the amount you would like to lock and click Done. You can now approve your lock.

Once you've approved your lock, you will be shown a History view where you will see your locks, funds you allocated to different projects, and user rewards you withdrew.

You can adjust your lock at any time. However, please bear in mind:
- You earn user rewards only from amounts of 100 GLM or more.
- If you reduce your lock before the end of the current epoch, your time-weighted average will be adjusted to reflect the smallest locked amount.

The Earn tab also allows you to withdraw your claimed rewards to your wallet. Please note however that withdrawals are possible only after the end of the current allocation window. 

There's no set deadline for withdrawing your claimed user rewards, allowing you flexibility in managing your assets.

## Metrics

In the Metrics view you can check the following information:
- when the current epoch allocation period ends;
- the total amount of ETH staked by the Golem Foundation and GLM time-locked by Octant users;
- the fraction of GLM time-locked by Octant users in relation to the Total GLM supply;
- the current value of ETH rewards transferred by Golem Foundation to Octant, and the value of users and matched donations to community chosen projects; 
- how much money has been claimed by community users versus donated to eligible projects;
- information on the amount of GLM tokens that are unclaimed and unallocated at the end of the epoch, or allocated to projects below the cutoff threshold. 

[launching-octant]: https://octant.app
[walletconnect]: https://explorer.walletconnect.com/?type=wallet
[terms-of-use]: ./terms-of-use.md
[glm-locking]: ./glm-locking.md
[glm]: https://www.golem.network/glm
[metamask]: https://metamask.io/
[discord-help]: https://discord.com/channels/1066118789479010354/1066148242296930304
[discord-feedback]: https://discord.com/channels/1066118789479010354/1079777506502123572
[golem-network]: https://golem.network
[exchanges]: https://glm.golem.network/
[uniswap]: https://app.uniswap.org/#/swap?outputCurrency=0x7DD9c5Cba05E151C895FDe1CF355C9A1D5DA6429
[effective]: ./faq.md#what-is-the-effective-locked-balance
[coingecko]: https://www.coingecko.com/en/api
[propose-a-project]: ./propose-a-project.md
[technical-outline]: ./technical-outline.md
[how-it-works]: ./how-it-works.md
