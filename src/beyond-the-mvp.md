# Beyond the MVP

The version of Octant [that has been deployed on the mainnet](https://octant.app) is only an MVP (a minimum viable product). In more mature versions of the platform, we want to extend the scope of community-driven governance and make it available to outside entities whishing to test their novel governance ideas in a real-life setting. 

You could even imagine Octant as an Experiment-as-a-Service, where researchers and DAO-practitioners can gain access to an experimental playground with a pool of participants facing actual economic incentives in exchange for doing something beneficial for the [GLM][GLM] ecosystem.

This is how a service like this could work:

1. [Golem Foundation][golem-foundation] allocates a certain amount of ETH to Octant from its staking rewards in a particular period.
2. There is an open call for proposals to design a governance experiment that eventually distributes the staking rewards of that period among Octant participants.
3. In order to submit a proposal, one needs to lock a specific amount of GLM. The highest-bidding proposal wins (more complex auction frameworks can be considered as well). The GLM associated with the highest bid is burned, and thus permanently removed from the supply.  
5. If no proposals are submitted and whitelisted, an experiment proposed by Golem Foundation is implemented in a particular period.

We don't have a fixed roadmap for this post-MVP version of Octant yet. Octant's community can help us create it.

The earlier you join, the more opportunity you will have to shape the platform and propose new features. Help us envision:
- how matching funding could be distributed in the future;
- delegating roles;
- improving DAOs with Golem Foundation's [User-Defined Organization][UDO];
- testing anti-Sybil mechanisms;
- testing Ethereum layer 2 solutions;
- a reputation system using Soulbound Tokens;
- finding exciting use cases for utility NFTs, and for the [Golem Network's][golem-network] native GLM token.

Join the community to help us shape the future of Octant:

- Follow us on [Twitter][twitter] and [Mastodon][mastodon]
- Join us on [Discord][discord] and the [Octant Snapshot][snapshot]
- If you are a developer, take a look at our [GitLab][gitlab]

[golem-foundation]: https://golem.foundation
[UDO]: https://wildland.io/2021/12/16/udo2.html
[golem-network]: https://golem.network
[glm]: https://www.golem.network/glm 
[twitter]: https://twitter.com/OctantApp/
[mastodon]: https://fosstodon.org/@TODO
[snapshot]: https://snapshot.org/#/Octantapp.eth 
[gitlab]: https://gitlab.com/wildland/governance/octant 
[discord]: https://discord.com/invite/aKNj2p9h59/login 
